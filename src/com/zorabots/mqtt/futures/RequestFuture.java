package com.zorabots.mqtt.futures;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import com.zorabots.mqtt.MqttPayload;
import com.zorabots.mqtt.MqttRequestCallback;

public class RequestFuture extends MqttRequestCallback implements Future<MqttPayload> {

	private volatile MqttPayload result;
	private volatile boolean cancelled = false;
	private final CountDownLatch countDownLatch;
	
	public RequestFuture() {
		countDownLatch = new CountDownLatch(1);
	}
	
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		if (isDone()) {
            return false;
        } else {
            countDownLatch.countDown();
            cancelled = true;
            return !isDone();
        }
	}

	@Override
	public MqttPayload get() throws InterruptedException, ExecutionException {
		countDownLatch.await();
		return result;
	}

	@Override
	public MqttPayload get(long timeout, TimeUnit unit) throws InterruptedException,
			ExecutionException, TimeoutException {
		countDownLatch.await(timeout, unit);
		return result;
	}

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public boolean isDone() {
		return countDownLatch.getCount() == 0;
	}

	@Override
	public void responseArrived(String topic, MqttPayload message) {
		this.result = message;
		countDownLatch.countDown();
	}
	
}
