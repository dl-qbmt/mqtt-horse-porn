package com.zorabots.mqtt;


public abstract class MqttRequestCallback {
	public abstract void responseArrived(String topic, MqttPayload message);
	
    public void timeoutExceeded() {
    	
    }
    
    public void errorOccured(String reason) {
    	
    }
}
