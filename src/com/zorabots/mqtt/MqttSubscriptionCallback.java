package com.zorabots.mqtt;


public interface MqttSubscriptionCallback {
	void messageArrived(String topic, MqttPayload message);
}
