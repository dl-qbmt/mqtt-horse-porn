package com.zorabots.mqtt;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.zorabots.mqtt.futures.RequestFuture;

public class Mqtt {
	private static final String BROKER_IP = "127.0.0.1";
	private static final String BROKER_PORT = "1883";

    public static void makeRequest(final String requestTopic, final String responseTopic, String payload, final MqttRequestCallback callback, final long timeout) {
        final MqttInstance instance = MqttInstance.getInstance();

        final Thread timeoutThread = new Thread(new Runnable() {
            public void run() {
                try {
                    Thread.sleep(timeout);
                    
                    callback.timeoutExceeded();
                } catch (InterruptedException e) {
                    System.out.println("Timeout interrupted due to response being received.");
                }
                
                instance.unsubscribe(responseTopic);
            }
        });

        try {
            IMqttToken token = instance.subscribe(responseTopic, new IMqttMessageListener() {
                public void messageArrived(String topic, MqttMessage mqttMessage) throws Exception {
                    timeoutThread.interrupt();
                    instance.unsubscribe(requestTopic);
                    callback.responseArrived(topic, new MqttPayload(mqttMessage));
                }
            });

            token.waitForCompletion();

            if (token.isComplete()) {
                instance.publish(requestTopic, payload);

                timeoutThread.start();
            } else {
                callback.errorOccured("Subscription failed");
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public static void makeRequest(final String requestTopic, final String responseTopic, String payload, final MqttRequestCallback callback) {
        Mqtt.makeRequest(requestTopic, responseTopic, payload, callback, 5000L);
    }
    
    public static RequestFuture makeRequest(final String requestTopic, final String responseTopic, String payload) {
    	RequestFuture future = new RequestFuture();
    	
    	Mqtt.makeRequest(requestTopic, responseTopic, payload, future, Long.MAX_VALUE);
    	
    	return future;
    }

    public static void publish(String topic, String payload) {
        final MqttInstance instance = MqttInstance.getInstance();

        instance.publish(topic, payload);
    }

    public static void subscribe(String topic, final MqttSubscriptionCallback callback) {
        final MqttInstance instance = MqttInstance.getInstance();

        try {
            instance.subscribe(topic, new IMqttMessageListener() {

				@Override
				public void messageArrived(String topic, MqttMessage message)
						throws Exception {
					callback.messageArrived(topic, new MqttPayload(message));
				}
            	
            });
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public static void unsubscribe(String topic) {
        final MqttInstance instance = MqttInstance.getInstance();

        instance.unsubscribe(topic);
    }

    public static void startClient() {
    	Mqtt.startClient(BROKER_IP, BROKER_PORT);
    }
    
    public static void startClient(String brokerIp, String brokerPort) {
    	final MqttInstance instance = MqttInstance.getInstance();
    	
    	instance.startClient(String.format("tcp://%s:%s", brokerIp, brokerPort));
    }

    public void stopClient() {
        final MqttInstance instance = MqttInstance.getInstance();
        instance.stopClient();
    }
}
