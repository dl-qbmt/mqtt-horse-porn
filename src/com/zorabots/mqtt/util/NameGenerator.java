package com.zorabots.mqtt.util;

import java.util.Random;

public class NameGenerator {

    private static final String uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static final String lowercase = uppercase.toLowerCase();
    private static final String digits = "0123456789";
    private static final String additions = "_-";

    public static final String alphanumerics = uppercase + lowercase + digits + additions;

    private final Random random;

    private final char[] symbols;

    private char[] buffer;

    public NameGenerator() {
        random = new Random();
        symbols = alphanumerics.toCharArray();
    }

    public String generateName(String prefix, int length) {
        if (length < 0) {
            throw new IllegalArgumentException("generateName expected a positive or zero length");
        }
        char[] buffer = new char[length];

        for (int i = 0; i < length; i++) {
            buffer[i] = symbols[random.nextInt(symbols.length)];
        }

        return prefix + new String(buffer);
    }

    public String generateName(int length) {
        return generateName("", length);
    }
}

