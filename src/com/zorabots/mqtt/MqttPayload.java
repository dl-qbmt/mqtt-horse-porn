package com.zorabots.mqtt;

import org.eclipse.paho.client.mqttv3.MqttMessage;

public class MqttPayload {
	private MqttMessage message;

	public MqttPayload(MqttMessage message) {
		this.message = message;
	}
	
	public byte[] getPayloadBytes() {
		return message.getPayload();
	}
	
	public String getPayload() {
		return new String(message.getPayload());
	}
}
