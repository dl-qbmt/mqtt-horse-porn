package com.zorabots.mqtt;

import java.util.logging.Logger;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import com.zorabots.mqtt.util.NameGenerator;

public class MqttInstance {
	private static class InstanceHolder {
        private static final MqttInstance instance = new MqttInstance();
    }

    private MqttClient client;
    private boolean running = false;
    private Logger logger;

    private MqttInstance() {
    	logger = Logger.getLogger(MqttInstance.class.getSimpleName());
    }

    public static MqttInstance getInstance() {
        return InstanceHolder.instance;
    }

    public void startClient(final String brokerUrl) {
        if (!running) {
        	logger.info("Connecting to client: " + brokerUrl);

            running = true;

            String clientId = (new NameGenerator()).generateName("mqtt-odp-", 5);

            MqttConnectOptions options = new MqttConnectOptions();
            options.setCleanSession(true);

            try {
                logger.info("Creating MQTT client...");

                client = new MqttClient(brokerUrl, clientId);
                client.setCallback(new MqttCallback() {
                	@Override
                    public void connectionLost(Throwable throwable) {
                        try {
                            client.reconnect();
                        } catch (MqttException e) {
                            e.printStackTrace();
                        }
                    }
					@Override
					public void deliveryComplete(IMqttDeliveryToken arg0) {
						// TODO Auto-generated method stub
						
					}

					@Override
					public void messageArrived(String arg0,
							MqttMessage arg1) throws Exception {
						// TODO Auto-generated method stub
						
					}
                });
                IMqttToken connectToken = client.connectWithResult(options);
                
                connectToken.waitForCompletion(2000L);
                
                if (connectToken.isComplete()) {
                	System.out.println("MQTT connected to " + brokerUrl);
                } else {
                	System.out.println("MQTT connection failed to " + brokerUrl);
                }

            } catch (MqttException exception) {
                System.out.println("MQTT client connection threw an exception.");
                exception.printStackTrace();
            }
        }
    }

    public IMqttToken subscribe(String topic, IMqttMessageListener listener) throws MqttException {

        if (running) {
            return client.subscribeWithResponse(topic, listener);
        }

        return null;
    }

    public void unsubscribe(String topic) {
        if (running) {
            try {
                client.unsubscribe(topic);
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    public void publish(String topic, String payload) {
        MqttMessage message = new MqttMessage(payload.getBytes());
        message.setQos(0);

        try {
            client.publish(topic, message);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void stopClient() {
        if (running) {
            try {
                client.disconnect();
                running = false;
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }
}
